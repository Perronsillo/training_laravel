<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Softdeletes;
class Post extends Model
{
    //propiedad para eliminacion por softdelete
    use Softdeletes;
    protected $dates = ['deleted_at'];

    //Propiedades del modelo Post
    protected $fillable = [
        'title',
        'content'
    ];
      //función para mostrar los registros relacionas de un post en usuario
      public function user(){
        return $this->belongsTo('App\User');
    }
}

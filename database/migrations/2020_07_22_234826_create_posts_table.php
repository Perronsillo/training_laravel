<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    // Función para la creación de la tabla posts para trabajar en mi entrenamiento
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id')->insigned();
            $table->string('title');
            $table->string('content');
            $table->timestamps();
        });
    }

    // Función para la eliminación de la tabla si ya existe
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
